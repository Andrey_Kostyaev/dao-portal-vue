import Vue from "vue";
import Router from "vue-router";
import Dashboard from "@/views/Dashboard/Dashboard.vue";
import CreateManual from "@/components/manuals/CreateManual/CreateManual.vue"
import Account from "@/views/Account/Account.vue"

import routes from '@/types/routes'

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: routes.home,
      name: "dashboard",
      component: Dashboard,
    },
    {
      path: routes.manuals,
      name: "manuals",
      component: Dashboard,
    },
    {
      path: routes.manualCreate,
      name: "manualsCreate",
      component: CreateManual,
    },
    {
      path: routes.account,
      name: "account",
      component: Account,
    }
  ]
});
