import types from "./types"

export default {
  [types.GET_USER_ACCOUNT_SUCCESS](state:any, data: object) {
    state.account = {
      ...state.account,
      ...data,
    }
  },
  [types.SET_USERS](state:any, data: Array<Object>) {
    state.items = {
      ...state.items,
      ...data,
    }
  }
}