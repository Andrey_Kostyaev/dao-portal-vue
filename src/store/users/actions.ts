import * as R from "ramda"

import api from "@/utils/api"
import {
  getUserId,
} from "@/helpers/app"

import TestImg from '@/assets/icons/test-img.jpg'

import urls from "@/request/urls"

import types from "./types"

export default {
  async [types.GET_USER_ACCOUNT]({ commit }:any) {
    try {
      const userId = getUserId()
      const result = await api.get(urls.userById(userId as string))
      const { roles, projects, feedback, daoRoles, username } = result
      await commit(types.GET_USER_ACCOUNT_SUCCESS, {
        roles,
        projects,
        feedback,
        daoRoles,
        username,
      })
    } catch(error) {

    }
  },
  async [types.GET_ALL_USERS]({ commit }:any) {
    try {
      const result = await api.get(urls.users)
      const itemsWithImgTest = R.map(user => {
        user.img = TestImg
        return user
      }, result)
      await commit(types.SET_USERS, itemsWithImgTest)
    } catch (error) {
      console.log(error)
    }
  },
}