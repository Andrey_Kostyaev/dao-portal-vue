import types from "./types"

export default {
  [types.GET_USERNAME]: (state:any) => state.account.username,
  [types.GET_USERS]: (state: any) : Array<Object> => state.items,
}