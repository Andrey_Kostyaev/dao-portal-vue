import * as R from "ramda"

import api from "@/utils/api"

import urls from "@/request/urls"

import types from "./types"

export default {
  async [types.API_GET_MANUALS]({ commit }:any) {
    try {
      const result = await api.get(urls.manuals)
      commit(types.SET_MANUALS, result)
    } catch(error) {
      console.log(error)
    }
  },
}