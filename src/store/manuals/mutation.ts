import types from './types'

export default {
  [types.SET_MANUALS]: (state: any, data: Array<Object>) => {
    state.items = data
  },
}