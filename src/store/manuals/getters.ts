import types from './types'

export default {
  [types.GET_MANUALS]: (state: any) => state.items,
}