import Vue from "vue";
import Vuex from "vuex";

import app from './app/index'
import auth from './auth/index'
import users from './users/index'
import manuals from './manuals/index'
import events from './events/index'
import tags from './tags/index'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    app,
    auth,
    users,
    manuals,
    events,
    tags,
  },
});
