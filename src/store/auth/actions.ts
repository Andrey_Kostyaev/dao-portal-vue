import * as R from "ramda"

import api from "@/utils/api"
import {
  setAccessToken, removeAccessToken, setUserId, removeUserId, getUserId,
} from "@/helpers/app"

import types from "./types"

export default {
  async [types.SIGN_IN]({ state, commit }:any) {
    try {
      const { email, password } = state
      const result = await api.auth({ email, password })
      setAccessToken(R.prop("token", result))
      setUserId(R.path(["data", "id"], result))
      commit(types.SET_STATUS, true)
      commit(types.CLEAR_AUTH)
    } catch(error) {
      commit(types.SET_ERROR, R.pathOr("null", ["response", "data", "message"], error))
    }
  },
  [types.LOGOUT]({ commit }:any) {
    removeAccessToken()
    removeUserId()
    commit(types.SET_STATUS, false)
  }
}