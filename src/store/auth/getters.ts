import types from "./types"

export default {
  [types.GET_EMAIL]: (state: { email: string; }) => state.email,
  [types.GET_PASSWORD]: (state: { password: string; }) => state.password,
  [types.GET_ERROR]: (state: { error: string; }) => state.error,
  [types.GET_STATUS]: (state: { isAuth: boolean; }) => state.isAuth,
}