import types from "./types"

export default {
  [types.SET_EMAIL](state: { email: string, error: string }, value:string) {
    state.email = value;
    state.error = "";
  },
  [types.SET_PASSWORD](state: { password: string, error: string }, value:string) {
    state.password = value;
    state.error = "";
  },
  [types.SET_ERROR](state: { error: string}, value:string) {
    state.error = value;
  },
  [types.CLEAR_AUTH](state:any) {
    state.error = null
    state.email = ""
    state.password = ""
  },
  [types.SET_STATUS](state: { isAuth: boolean; }, value:boolean) {
    state.isAuth = value;
  }
}