import defaultState from "./state"
import mutations from "./mutation"
import getters from "./getters"
import actions from "./actions"

export default {
  namespaced: true,
  state: { ...defaultState },
  mutations,
  getters,
  actions,
}