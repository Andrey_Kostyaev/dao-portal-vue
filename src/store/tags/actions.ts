import api from "@/utils/api"

import urls from "@/request/urls"

import types from './types'

export default {
  async [types.API_TAGS_GET]() {
    try {
      const result = api.get(urls.tags)
      console.log(result)
    } catch (error) {
      console.log(error) 
    }
  }
}