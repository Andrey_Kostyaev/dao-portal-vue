import types from './types'
import type from 'ramda/es/type';

export default {
  [types.SET_EVENTS]: (state: any, data: Array<Object>) => {
    state.items = data
  },
  [types.SET_FIELD_NEW_EVENT]: (state:any, data: {field: string, value: any}) => {
    const { field, value } = data
    state.newEvent[field] = value
  }
}