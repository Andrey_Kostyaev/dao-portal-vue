import * as R from "ramda"

import api from "@/utils/api"

import urls from "@/request/urls"

import types from './types'

export default {
  async [types.API_GET_EVENTS]({ commit } : any) {
    try {
      const result = await api.get(urls.events)
      commit(types.SET_EVENTS, result)
    } catch(error) {
      console.log(error)
    }
  }
}