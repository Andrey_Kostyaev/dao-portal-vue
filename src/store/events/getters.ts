import types from './types'

export default {
  [types.GET_EVENTS]: (state: any) => state.items,
  [types.GET_FIELD_NEW_EVENT]: (state: any)  => (field: string) => state.newEvent[field],
}