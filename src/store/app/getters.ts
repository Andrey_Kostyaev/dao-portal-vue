import types from "./types"

export default {
  [types.GET_INIT]: (state: { isInitDone: boolean }) => state.isInitDone,
}