import manualsTypes from '@/store/manuals/types'
import eventsTypes from '@/store/events/types'
import userTypes from "@/store/users/types"
import tagsTypes from "@/store/tags/types"
import types from "./types"

export default {
  async [types.INIT](data:any) {
    try {
      const { dispatch, commit } = data
      await dispatch(`users/${userTypes.GET_USER_ACCOUNT}`, null, { root: true })
      await dispatch(`users/${userTypes.GET_ALL_USERS}`, null, { root: true})
      await dispatch(`manuals/${manualsTypes.API_GET_MANUALS}`, null, { root: true })
      await dispatch(`events/${eventsTypes.API_GET_EVENTS}`, null, { root: true })
      //await dispatch(`tags/${tagsTypes.API_TAGS_GET}`, null, { root: true })
      await commit(types.SET_INIT, true)
    } catch(error) {
      console.log(error)
    }
  },
}