import { Component, Emit, Vue } from "vue-property-decorator";
import { namespace } from 'vuex-class';

import Auth from '@/views/Auth/Auth.vue';
import Portal from '@/views/Portal/Portal.vue';

import { isAuthUser } from '@/helpers/app';
import types from '@/store/auth/types'

const authModule = namespace('auth')

@Component({
  components: {
    Auth,
    Portal,
  }
})

export default class App extends Vue {
  @authModule.Getter(types.GET_STATUS) status!:boolean;
  @authModule.Mutation(types.SET_STATUS) setStatus!:Function
  mounted() {
    this.setStatus(isAuthUser())
  }
}