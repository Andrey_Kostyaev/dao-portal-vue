const urls = {
    events: '/events',
    feedbacks: '/feedback',
    librarys: '/libraries',
    manuals: '/manuals',
    tags: '/tags',
    projects: '/projects',
    questionarys: '/questionaries',
    users: '/users',
  }
  
  export default {
    ...urls,
    eventById: (id:string | number) => (`${urls.events}/${id}`),
    feedbackById: (id:string | number) => (`${urls.feedbacks}/${id}`),
    libraryById: (id:string | number) => (`${urls.librarys}/${id}`),
    manualById: (id:string | number) => (`${urls.manuals}/${id}`),
    projectById: (id:string | number) => (`${urls.projects}/${id}`),
    questionaryById: (id:string | number) => (`${urls.questionarys}/${id}`),
    userById: (id:string | number) => (`${urls.users}/${id}`),
    tagById: (id:string | number) => (`${urls.tags}/${id}`),
  }