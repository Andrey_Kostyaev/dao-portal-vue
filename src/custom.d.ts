declare module "*.svg" {
  const content: any;
  export default content;
}

declare module "*.png" {
  const content: any;
  export default content;
}

declare module "*.jpg" {
  const content: any;
  export default content;
}

declare module "@fullcalendar/vue" {
  const content: any;
  export default content;
}

declare module "@fullcalendar/core/locales/ru" {
  const content: any;
  export default content;
}