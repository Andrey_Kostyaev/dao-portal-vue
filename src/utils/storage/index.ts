import * as R from 'ramda'

const storage = localStorage
const prefix = 'dao-portal.'

const withPrefix = (name:string) => prefix + name

export default {
  set: (name:string, value:string) => storage.setItem(withPrefix(name), value),
  get: (name:string) => storage.getItem(withPrefix(name)),
  remove: (name:string) => storage.removeItem(withPrefix(name)),
  has: (name: string) => R.includes(withPrefix(name), R.keys(storage))
}