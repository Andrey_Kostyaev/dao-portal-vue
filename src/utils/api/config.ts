import { AxiosRequestConfig } from "axios";

import { getAccessToken } from "@/helpers/app";

const config:AxiosRequestConfig = {
  baseURL: process.env.VUE_APP_API_URL,
  responseType: "json",
  headers: {
    Authorization: getAccessToken(),
    Accept: "application/json",
    "Content-Type": "application/json"
  },
}

export default config