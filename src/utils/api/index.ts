import axios, { AxiosRequestConfig } from "axios";

import config from "./config";

const api = axios.create(config);

const basic = (url: string, method: any, data: object = {}) =>{
  const conf:AxiosRequestConfig = {
    method,
    url,
    data,
  }
  return api(conf).then((response: { data: any; }) => response.data)
};

export default {
  auth: (data:object) => basic("/login_check", "post", data),
  post: (url:string, data:object) => basic(`/api${url}`, "post", data),
  get: (url:string) => basic(`/api${url}`, "get"),
  delete: (url:string) => basic(`/api${url}`, "delete"),
  put: (url:string, data:object) => basic(`/api${url}`, "put", data)
};
