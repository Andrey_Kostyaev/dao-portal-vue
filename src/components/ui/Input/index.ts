import { Component, Prop, Vue, Emit } from "vue-property-decorator";

@Component
export default class Input extends Vue {
  @Prop({ default: 'text' }) type!:string;
  @Prop() placeholder!:string;
  @Prop() value!:string;
  @Prop({ default: null }) label!:string | null;


  @Emit('input')
  callback(e: { target: { value: any; }; }) {
    return e.target.value
  }
}