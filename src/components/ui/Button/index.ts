import { Component, Prop, Vue, Emit } from "vue-property-decorator";

@Component
export default class Button extends Vue {
  @Prop({ default: '' }) text!:string;
  @Prop({ default: 'button' }) type!:string;
  @Prop({ default: false }) disabled!:boolean;

  @Emit('click')
  callback(e: any) {
    e.preventDefault()
    return e;
  }
}