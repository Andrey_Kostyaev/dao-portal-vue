import { Component, Vue } from "vue-property-decorator";

import MenuIcon from '@/assets/icons/menu.svg'

@Component({
  components: {
    MenuIcon,
  }
})
export default class BurgerMenu extends Vue {
}