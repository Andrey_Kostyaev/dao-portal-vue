import { Component, Vue, Emit } from "vue-property-decorator";
import { namespace } from 'vuex-class';

import FullCalendar from '@fullcalendar/vue'
import dayGridPlugin from '@fullcalendar/daygrid'
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import ruLocale from '@fullcalendar/core/locales/ru';

import eventsTypes from '@/store/events/types'

const eventsModule = namespace('events')

@Component({
  components: {
    FullCalendar,
  },
  data: () => ({
    ruLocale,
    calendarPlugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
    },
    selectable: true,
  })
})
export default class Events extends Vue {
  @eventsModule.Getter(eventsTypes.GET_EVENTS) events!: Array<Object>
  @eventsModule.Getter(eventsTypes.GET_FIELD_NEW_EVENT) getFieldNewEvent!: Function
  @eventsModule.Mutation(eventsTypes.SET_FIELD_NEW_EVENT) setFieldNewEvent!: Function

  @Emit('Events-select-date')
  handleSelect(data: { start:Date, end:Date, allDay:boolean}) {
    console.log(typeof this.getFieldNewEvent)
    const {start, end, allDay} = data
    this.setFieldNewEvent({field: 'start', value: start})
    this.setFieldNewEvent({field: 'end', value: end})
    this.$refs['event-new-modal'].show()
  }
}