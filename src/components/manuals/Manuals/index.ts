import { Component, Vue } from "vue-property-decorator";
import { namespace } from 'vuex-class';

import manualsTypes from '@/store/manuals/types'

const manualsModule = namespace('manuals')

@Component
export default class Manuals extends Vue {
  @manualsModule.Getter(manualsTypes.GET_MANUALS) items!:Array<Object>
}