import { Component, Vue, Prop } from "vue-property-decorator";

@Component
export default class Item extends Vue {
  @Prop() item!: { firstName: string; lastName: string; img: string };
  name = `${this.item.firstName} ${this.item.lastName}`;
}
