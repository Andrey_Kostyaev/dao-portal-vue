import { Component, Vue } from "vue-property-decorator";
import { namespace } from 'vuex-class';

import Item from "./Item/Item.vue"

import usersTypes from '@/store/users/types'

const usersModule = namespace('users')

@Component({
  components: {
    Item,
  }
})
export default class Adepts extends Vue {
  @usersModule.Getter(usersTypes.GET_USERS) users !:Array<object>
}