import { Component, Vue, Emit } from "vue-property-decorator";
import { namespace } from 'vuex-class';

import Logo from '@/assets/icons/dao-logo.svg'

import usersTypes from '@/store/users/types'
import authTypes from '@/store/auth/types'

import routes from '@/types/routes'

const usersModule = namespace('users')
const authModule = namespace('auth')

@Component({
  components: {
    Logo,
  },
  data: () => ({
    routes,
  })
})
export default class NavBar extends Vue {
  @usersModule.Getter(usersTypes.GET_USERNAME) username!:string;
  @authModule.Action(authTypes.LOGOUT) logout!:Function
  @Emit('click-logout')
  handlerClickExit() {
    this.logout();
  }
}