import { Component, Vue, Prop } from "vue-property-decorator";

@Component
export default class Item extends Vue {
  @Prop() item!:{ name: string, img: string };
}