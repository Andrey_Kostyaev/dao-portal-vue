import SlackIcon from '@/assets/icons/slack-icon.png'
import JiraIcon from '@/assets/icons/jira-icon.png'
import ZeplinkIcon from '@/assets/icons/zeplin-icon.png'
import SketchIcon from '@/assets/icons/sketch-icon.png'
import BitbucketIcon from '@/assets/icons/bitbucket-icon.png'
import GihubIcon from '@/assets/icons/github-icon.png'

export default [
  {
    name: 'Slack',
    img: SlackIcon,
    href: 'https://daotech.slack.com/',
  },
  {
    name: 'Jira',
    img: JiraIcon,
    href: 'https://dao-tech.atlassian.net/',
  },
  {
    name: 'Zeplin',
    img: ZeplinkIcon,
    href: 'https://app.zeplin.io',
  },
  {
    name: 'Sketch',
    img: SketchIcon,
    href: 'https://www.sketch.com/',
  },
  {
    name: 'Bitbucket',
    img: BitbucketIcon,
    href: 'https://bitbucket.org/',
  },
  {
    name: 'Github',
    img: GihubIcon,
    href: '#',
  }
]