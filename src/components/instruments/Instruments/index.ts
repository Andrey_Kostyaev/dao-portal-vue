import { Component, Vue } from "vue-property-decorator";

import Item from './Item/Item.vue'

import config from './config'

@Component({
  components: {
    Item,
  },
  data: () => ({
    config,
  })
})
export default class Instruments extends Vue {

}