enum routes {
  home = "/",
  account = "/account",
  manuals = "/manuals",
  manualCreate = "/manuals/create",
}

export default routes