import BasicInterface from './basic'
import TagInterface from './tag'

interface Event extends BasicInterface {
  name: string;
  start: string;
  end: string;
  policity: 'Public' | 'Protected' | 'Private'
  description?: string
  tags?: Array<TagInterface>
}

export default Event