interface Basic {
  id: string;
  createdAt: string;
  updatedAt: string;
  createdById: string;
  updatedById: string;
}

export default Basic