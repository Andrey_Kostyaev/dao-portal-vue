import BasicInterface from './basic'
import EventInterface from './event'

interface Tag extends BasicInterface {
  name: string,
  daoBonus?: string
  events?: Array<EventInterface>
  libraries?: Array<object>
  manuals?: Array<object>
  news?: Array<object>
  purchases?: Array<object>
}

export default Tag