import * as R from 'ramda'

import storage from '@/utils/storage'

import types from '@/types/user'

export const setAccessToken = (value:string) => storage.set(types.assessToken, value)
export const getAccessToken = () => `Bearer ${storage.get(types.assessToken)}`
export const removeAccessToken = () => storage.remove(types.assessToken)
const hasAccessToken = () => storage.has(types.assessToken)

export const setUserId = (value:string|number|undefined) => storage.set(types.userId, R.toString(value))
export const getUserId = () => storage.get(types.userId)
export const removeUserId = () => storage.remove(types.userId)
const hasUserId = () => storage.has(types.userId)

export const isAuthUser = () => (R.all(R.equals(true))([hasAccessToken(), hasUserId()]))