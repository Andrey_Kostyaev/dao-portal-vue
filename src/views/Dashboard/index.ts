import { Component, Vue } from "vue-property-decorator";

import Instruments from '@/components/instruments/Instruments/Instruments.vue'
import Manuals from '@/components/manuals/Manuals/Manuals.vue'
import Events from '@/components/events/Events/Events.vue'
import Adepts from '@/components/adepts/Adepts/Adepts.vue'

import Card from '@/components/ui/Card/Card.vue'
import BurgerMenu from '@/components/ui/BurgerMenu/BurgerMenu.vue'

@Component({
  components: {
    Card,
    BurgerMenu,
    Instruments,
    Manuals,
    Events,
    Adepts,
  }
})
export default class Dashboard extends Vue {
  
}