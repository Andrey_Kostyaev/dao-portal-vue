import { Component, Vue } from "vue-property-decorator";
import { namespace } from 'vuex-class';

import Page from '@/views/Page/Page.vue'

import NavBar from '@/components/navbar/NavBar/NavBar.vue';
import Card from '@/components/ui/Card/Card.vue'

import types from '@/store/app/types'

const appModule = namespace('app')

@Component({
  components: {
    NavBar,
    Card,
    Page,
  }
})
export default class Portal extends Vue {
  @appModule.Action(types.INIT) initApp!:Function;
  @appModule.Getter(types.GET_INIT) isInit!:boolean;
  mounted() {
    this.initApp();
  }
}