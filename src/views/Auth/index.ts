import { Component, Emit, Vue } from "vue-property-decorator";
import { namespace } from 'vuex-class'

import Card from '@/components/ui/Card/Card.vue'
import Input from '@/components/ui/Input/Input.vue'
import Button from '@/components/ui/Button/Button.vue'

import types from '@/store/auth/types'

const authModule = namespace('auth')

@Component({
    components: {
      Card,
      Input,
      Button,
    },
  })
export default class Auth extends Vue {
  @authModule.Getter(types.GET_EMAIL) email!:string
  @authModule.Getter(types.GET_PASSWORD) password!:string
  @authModule.Getter(types.GET_ERROR) error !: string
  @authModule.Mutation(types.SET_EMAIL) setEmail!:Function
  @authModule.Mutation(types.SET_PASSWORD) setPassword!:Function
  @authModule.Action(types.SIGN_IN) login!:Function
  @Emit()
  handlerOnClick(){
    const { email, password } = this
   this.login({ email, password })
  }
}